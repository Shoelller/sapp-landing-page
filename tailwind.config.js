/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./*.html"],
  theme: {
    screens:{
      '2xl': {'max': '2559px'},
      'xl': {'max': '1439px'},
      'lg': {'max': '1023px'},
      'md': {'max': '767px'},
      'sm': {'max': '479px'},
    },
    extend: {
      fontFamily:{
        'poppins':'Poppins'
      }
    },
  },
  plugins: [],
}
